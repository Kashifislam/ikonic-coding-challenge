<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\SocialConnectionController;

Route::middleware(['auth'])->group(function () {
    Route::get('/suggestions', [SocialConnectionController::class, 'getAllSuggestions'])->name('suggestions');
    Route::post('/sendRequest', [SocialConnectionController::class, 'sendRequest'])->name('sendRequest');
    Route::get('/requests/{mode}', [SocialConnectionController::class, 'getAllRequests'])->name('requests');
    Route::post('/delete', [SocialConnectionController::class, 'deleteRequest'])->name('delete');
    Route::post('/accept', [SocialConnectionController::class, 'acceptRequest'])->name('accept');
    Route::get('/connections', [SocialConnectionController::class, 'getAllConnections'])->name('connections');

    
});