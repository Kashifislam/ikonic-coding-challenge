var skeletonId = 'skeleton';
var contentId = 'content';
var skipCounter = 0;
var takeAmount = 10;


function getRequests(mode) {
 
 
  var exampleVariable = [];
  var functionsOnSuccess = [
    [exampleOnSuccessFunction, [exampleVariable, 'response']]
  ];
  // show skeleton
  $('div#skeleton').removeClass("d-none");
  // hide content
  $("div#show-content").addClass("d-none");
   ajax('/requests/'+mode, 'GET',functionsOnSuccess);
   $( document ).ajaxComplete(function() {
    $("div#request-records").slice(0, takeAmount).removeClass("d-none"); 
  });
  
}

function getMoreRequests() {
  //get total length by class
  var totalRequest = $('div#request-records.d-none').length;
  //get last ten count
  var request=totalRequest-takeAmount;         
  var i = skipCounter;
  $('div#request-records.d-none').each(function(){
      i++;
     
      if(i>request)
      {
       
          $(this).removeClass("d-none");
      }
  });
  // load more hide if no values remaining
  if($("div#request-records.d-none").length == 0){
    $('#load_more_btn_parent').addClass('d-none');
  }
 
}

function getConnections() {
  var exampleVariable = [];
  var functionsOnSuccess = [
    [exampleOnSuccessFunction, [exampleVariable, 'response']]
  ];
  // show skeleton
  $('div#skeleton').removeClass("d-none");
  // hide content
  $("div#show-content").addClass("d-none");
   ajax('/connections', 'GET',functionsOnSuccess);
   $( document ).ajaxComplete(function() {
    $("div#connection-records").slice(0, takeAmount).removeClass("d-none"); 
  });
}

function getMoreConnections() {
   //get total length by class
   var totalConnection = $('div#connection-records.d-none').length;
   //get last ten count
   var connection=totalConnection-takeAmount;         
   var i = skipCounter;
   $('div#connection-records.d-none').each(function(){
       i++;
      
       if(i>connection)
       {
        
           $(this).removeClass("d-none");
       }
   });
   // load more hide if no values remaining
   if($("div#connection-records.d-none").length == 0){
     $('#load_more_btn_parent').addClass('d-none');
   }
}

function getConnectionsInCommon(userId, connectionId) {
  // your code here...
}

function getMoreConnectionsInCommon(userId, connectionId) {
  // Optional: Depends on how you handle the "Load more"-Functionality
  // your code here...
}

function getSuggestions() {
  var exampleVariable = [];
  
  var functionsOnSuccess = [
    [exampleOnSuccessFunction, [exampleVariable, 'response']]
  ];
  // show skeleton
  $('div#skeleton').removeClass("d-none");
  // hide content
  $("div#show-content").addClass("d-none");

   ajax('/suggestions', 'GET',functionsOnSuccess);

  //  hide values for load more which are greater than 10
   $( document ).ajaxComplete(function() {
    $("div#suggestion-records").slice(0, takeAmount).removeClass("d-none");
  });
   
}

function getMoreSuggestions() {
  //get total length by class
  var totalSuggestions = $('div#suggestion-records.d-none').length;
  //get last 10 count
  var getSuggestions=totalSuggestions-takeAmount;         
  var i = skipCounter;
  $('div#skeleton').removeClass("d-none");
  $('div#suggestion-records.d-none').each(function(){
      i++;
      if(i>getSuggestions)
      {
        $(this).removeClass("d-none");
      }
  });
  $('div#skeleton').addClass("d-none");
  // load more hide if no values remaining
  if($("div#suggestion-records.d-none").length == 0){
    $('#load_more_btn_parent').addClass('d-none');
  } 
}

function sendRequest(userId, suggestionId) {
  var form = ajaxForm([
    ['suggestionId', suggestionId],
  ]);

   ajax('/sendRequest', 'POST','', form);
   getSuggestions()
   
}

function deleteRequest(userId, requestId) {
  var form = ajaxForm([
    ['requestId', requestId],
  ]);
  var exampleVariable = 'sent';
  var functionsOnSuccess = [
    [getRequests, [exampleVariable, 'response']]
  ];
   ajax('/delete', 'POST',functionsOnSuccess, form);
}

function acceptRequest(userId, requestId) {
  var form = ajaxForm([
    ['requestId', requestId],
  ]);
  var received = 'received';
  var functionsOnSuccess = [
    [getRequests, [received, 'response']]
  ];
   ajax('/accept', 'POST',functionsOnSuccess, form);
}

function removeConnection(userId, connectionId) {
  var form = ajaxForm([
    ['requestId', connectionId],
  ]);
  var exampleVariable = '';
  var functionsOnSuccess = [
    [getConnections, [exampleVariable, 'response']]
  ];
   ajax('/delete', 'POST',functionsOnSuccess, form);
}

//  page refresh script 
$(function () {
  
  getSuggestions();
  
});