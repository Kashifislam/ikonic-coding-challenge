<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\User;  
use App\Models\SentRequest;  
use App\Models\ReceivedRequest;   

class SocialConnectionController extends Controller
{
    
    public function getAllSuggestions()
    {
        $authId = auth()->user()->id;
        $users = User::where('id', '<>', $authId)->where(function ($query) {
            $query->whereDoesntHave('sentRequest')
                  ->whereDoesntHave('receivedRequest');
        })
        ->get();
        $suggestions =  view('components.suggestion',compact('users'))->render();
       
        return response()->json([
            'content' => $suggestions
        ]);
    }

    public function sendRequest(Request $request){
        
        $connect = SentRequest::create([
            'user_id'=>auth()->user()->id,
            'received_by'=>$request->input('suggestionId')
        ]);
        return response()->json([
            'success' => 1
        ]);
    }

    public function getAllRequests($mode)
    {
        if($mode=='sent')
        {
            $requests = SentRequest::with('user')
                        ->where([
                                ['user_id', '=', auth()->user()->id],
                                ['status', '=', 'Pending']
                                ])->get();
            
        }
        if($mode=='received')
        {
            $requests =  SentRequest::with('user')->where([
                        ['received_by', '=', auth()->user()->id],
                        ['status', '=', 'Pending']
                        ])->get();
      
        }
        $content =  view('components.request',compact('requests','mode'))->render();
        return response()->json([
            'content' => $content
        ]);
    }

    public function deleteRequest(Request $request)
    {
       
        $isDelete = SentRequest::where('id',$request->input('requestId'))->delete();
        if($isDelete){
            return response()->json([
                'success' => 1
            ]);
        }
    }

    public function acceptRequest(Request $request)
    {
        SentRequest::where('id', $request->input('requestId'))
                    ->update(['status' => 'Accepted']);
        return response()->json([
            'success' => 1
        ]);
    }

    public function getAllConnections()
    {
        $connections =  SentRequest::with('user')->where('status','Accepted')->where(function ($q) {
                    $q->orWhere('user_id', '=',auth()->user()->id )
                      ->orWhere('received_by', '=', auth()->user()->id);
        })->get();
        $content =  view('components.connection',compact('connections'))->render();
        return response()->json([
            'content' => $content
        ]);
    }
}
