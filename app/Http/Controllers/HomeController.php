<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\User;  
use App\Models\SentRequest;  
use App\Models\ReceivedRequest;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        $sugestionCount = User::where('id', '<>', auth()->user()->id)->where(function ($query) {
            $query->whereDoesntHave('sentRequest')
                  ->whereDoesntHave('receivedRequest');
        })->count();
        $sentRequests = SentRequest::with('user')
                        ->where([
                                ['user_id', '=', auth()->user()->id],
                                ['status', '=', 'Pending']
                                ])->count();
        $receivedRequests =  SentRequest::with('user')->where([
                            ['received_by', '=', auth()->user()->id],
                            ['status', '=', 'Pending']
                            ])->count();
        $connectionsCount = SentRequest::with('user')->where('status','Accepted')->where(function ($q) {
                                        $q->orWhere('user_id', '=',auth()->user()->id )
                                          ->orWhere('received_by', '=', auth()->user()->id);
                            })->count();
        return view('home',compact('sugestionCount','sentRequests','receivedRequests','connectionsCount'));
    }
}
