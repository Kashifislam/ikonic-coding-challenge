<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class SentRequest extends Model
{
    use HasFactory;

     /**
     * The attributes that are mass assignable.
     *
     * @var array<int, string>
     */
    protected $fillable = [
        'user_id',
        'received_by',
        'status'
       
    ];

     /**
     * Get the user that owns the sent requests.
     */
    public function user()
    {
        return $this->belongsTo(User::class,'received_by','id');
    }

     /**
     * Get the user that owns the sent requests.
     */
    public function connectionCommon()
    {
        return $this->belongsToMany(User::class,'sent_requests','id','received_by');
    }

    /**
     * Get the user that owns the received requests.
     */
     public function userReceived()
    {
        return $this->belongsTo(User::class,'user_id','id');
    } 

}
