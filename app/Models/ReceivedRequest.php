<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class ReceivedRequest extends Model
{
    use HasFactory;

    /**
     * Get the user that owns the receivd request.
     */
    public function user()
    {
        return $this->belongsTo(User::class);
    }
}
