<?php

namespace Database\Seeders;

use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Eloquent\Factories\Factory;
use Illuminate\Database\Seeder;
use App\Models\User;
use App\Models\ReceivedRequest;
use App\Models\SentRequest;

class UsersSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        User::factory()
        ->has(ReceivedRequest::factory()->count(3))
        ->has(SentRequest::factory()->count(3))
        ->count(100)->create();
    }
}
