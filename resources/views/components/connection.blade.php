<div class="my-2 shadow text-white bg-dark p-1"  id="show-content">
  @foreach ($connections as $item)
  <div class="d-flex justify-content-between d-none" id="connection-records">
    <table class="ms-1">
      <td class="align-middle">{{$item->userReceived->name}}</td>
      <td class="align-middle"> - </td>
      <td class="align-middle">{{$item->userReceived->email}}</td>
      <td class="align-middle">
    </table>
    <div>
      <button style="width: 220px" id="get_connections_in_common_" class="btn btn-primary" type="button"
        data-bs-toggle="collapse" data-bs-target="#collapse_{{$item->id}}" aria-expanded="false" aria-controls="collapseExample">
        Connections in common ({{$item->connectionCommon->count()}})
      </button>
      <button  onclick="removeConnection({{$item->received_by}},{{$item->id}})" id="create_request_btn_" class="btn btn-danger me-1">Remove Connection</button>
    </div>
  

  </div>
  <div class="collapse" id="collapse_{{$item->id}}">
    @isset($item->connectionCommon)
    @foreach($item->connectionCommon as $value)
    <div id="content_" class="p-2">
      
      <x-connection_in_common :name='$value->name' :email='$value->email' />
    </div>
    @endforeach
    @endisset
  </div>
  @endforeach
  @if(count($connections) > 10)
        <div class="d-flex justify-content-center mt-2 py-3" id="load_more_btn_parent">
          <button class="btn btn-primary" onclick="getMoreConnections()" id="load_more_btn">Load more</button>
        </div>
  @endif
  
</div>
<div id="skeleton" class="d-none">
          @for ($i = 0; $i < 10; $i++)
            <x-skeleton />
          @endfor
        </div>
