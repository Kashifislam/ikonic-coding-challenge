<div class="my-2 shadow text-white bg-dark p-1" id="show-content">
  
    @foreach ($requests as $item)
    <div class="d-flex justify-content-between d-none" id="request-records">
    <table class="ms-1">
      <td class="align-middle">{{$item->user->name}}</td>
      <td class="align-middle"> - </td>
      <td class="align-middle">{{$item->user->email}}</td>
      <td class="align-middle">
    </table>
    <div>
      @if ($mode == 'sent')
        <button id="cancel_request_btn_" class="btn btn-danger me-1"
          onclick="deleteRequest({{$item->received_by}},{{$item->id}})">Withdraw Request</button>
      @else
        <button id="accept_request_btn_" class="btn btn-primary me-1"
          onclick="acceptRequest({{$item->received_by}},{{$item->id}})">Accept</button>
      @endif
    </div>
   
  </div>
  @endforeach
  @if(count($requests) > 10)
  <div class="d-flex justify-content-center mt-2 py-3" id="load_more_btn_parent">
          <button class="btn btn-primary" onclick="getMoreRequests()" id="load_more_btn">Load more</button>
        </div>
        @endif
</div>
