<div class="my-2 shadow  text-white bg-dark p-1" id="show-content">
  @foreach ($users as $item)
   <div class="d-flex justify-content-between d-none" id="suggestion-records">
    <table class="ms-1">
      <td class="align-middle">{{$item->name}}</td>
      <td class="align-middle"> - </td>
      <td class="align-middle">{{$item->email}}</td>
    </table>
    <div>
      <button onclick="sendRequest('{{auth()->user()->id}}','{{$item->id}}')" id="create_request_btn_{{$item->id}}" class="btn btn-primary me-1">Connect</button>
    </div>
  </div>
  @endforeach
 
  @if(count($users) > 10)
        <div class="d-flex justify-content-center mt-2 py-3" id="load_more_btn_parent">
          <button class="btn btn-primary" onclick="getMoreSuggestions()" id="load_more_btn">Load more</button>
        </div>
  @endif

</div>
<div id="skeleton" class="d-none">
          @for ($i = 0; $i < 10; $i++)
            <x-skeleton />
          @endfor
        </div>
